// swift-tools-version:5.2

import PackageDescription

let package = Package(
    name: "SwiftStudyBlog",
    products: [
        .executable(
            name: "SwiftStudyBlog",
            targets: ["SwiftStudyBlog"]
        )
    ],
    dependencies: [
        .package(name: "Publish", url: "https://github.com/johnsundell/publish.git", from: "0.7.0")
    ],
    targets: [
        .target(
            name: "SwiftStudyBlog",
            dependencies: ["Publish"]
        )
    ]
)